/**
 * lop Dictionary ke thua lop Word
 * bao gom 1 mang Word va so luong tu trong mang Word
 * @author Pham Thanh Hung, Nguyen Trong Lam
 * @version commandline
 */
public class Dictionary extends Word{
	public static Word[] words;  //mang Word
	protected static int NumberOfWords; // so phan tu trong mang
       // ham khoi tao Dictionary khong co tham so
        public Dictionary(){
            this.words=new Word[1000];
            this.NumberOfWords=1;
        }
        // ham setUp dung de set thuoc tinh cua lop Dictionary
	public static void  SetUp(int n) {
		words=new Word[n];
		NumberOfWords=n;
	}
}